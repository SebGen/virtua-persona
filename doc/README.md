# Documentation #

Here you can find the documentation of this application.

* `user_stories.md`: The user stories this application based its dev
  on
* `models.md`: The models you can found on the code, as classes and
  objects
* `vues.md`: About the views of this application
* `controller`: About the controller and how it interract with views
  and models
