# Models #

## Entities analysis ##

Based on user stories, we have this list of entities:
* Character Collection
* Character
* Emotion
* Render settings
* Text settings
* Paths where are the character emotion images

For each of thems, I create a model. 

`Character` and `Emotion` can be load or dump to json. `Character
Collection` let you manage a collection of `Character` stored locally.

## Character Collection ##

This represent a collection of multiple `Character`, stored somewhere.
For now, only locally.

This model have these object preperties:
* `name`: Simply the name of the collection
* `path`: The path where the data are stored


This model have these object public methods:
* `list_characters_names()`: Return the name of each stored `Character`,
  as a list of strings
* `load_character_from_name(name)`: Return a `Character` object
  corresponding to the `name`
* `save_character(character)`: Save the given `Character` object,
  erase if already exist
  
## Character ##

This represent one character.

This model have these object preperties:
* `name`: Simply the name of the character
* `emotions`: List of emotions, as a list on `Emotion` objects
* `render_settings`: The rendering settings for this character, as a
  `RenderSettings` object
* `text settings`: The text settings for this character, as a
  `TextSettings` object

## Emotion ##

This represent an emotion of a character, to be render when selected.

This model have these object preperties:
* `name`: Simply the name of the emotion
* `icon`: An icon, representing the emotion
* `images_paths`: An `EmotionImagesPaths` object, representing the
  four images paths
  
## EmotionImagesPaths ##

This represent the four images paths, one per mouth status:
* Mouth closed
* Mouth half closed
* Mouth opened 
* Mouth fully opened

This model have these object preperties:
* `mouth_open`: Path to the image representing the avatar with an emotion and mouth open
* `mouth_close`: Path to the image representing the avatar with an emotion and mouth close
* `mouth_half_open`: Path to the image representing the avatar with an emotion and mouth half open
* `mouth_fully_open`: Path to the image representing the avatar with an emotion and mouth fully open
