# Views #

Here you can found user interface


## Main view ##

Here, the user will find all actions he or she need to do wile he or
she do a live stream.

Its need to be simple and easy to do actions, because when the user
live stream, he or she are focused on something else.

These actions are:
* Select an emotion
* Write a text the user want to show as his or her words
* Select another character
* Select character position (right or left) and text position (top or
  bottom)


### Emotions ###

When user select an emotion, timing it the key to authenticity. So,
user need to identify an emotion immediately and select it quickly.

For that, emotion will have an emoticon and a name and all emotions of
a character are shown on a grid. 

For readability, a maximum of 10 emotions will be permitted at start.
In a future version, more emotions could be added and accessed with a
tree dots button.

As emotion selection is one of the most common action, if not the
most, the emotions will be shown as high as possible in the main view.


### Text ###

When user want to express by text, its important that this can be done
as quickly as possible and as naturally as possible. If the text
writing feel unnatural, or have "strange" behavior for the user, this
will make expressing more difficult for the user.

So, things like copy and past must work as in the majority of
applications. And a press on "enter" keyboard key must insert a new
line, like in other applications with multi-lines text writing.

As text writing is the second most common action, text entry will be
shown at the bottom of the main view to be easy to identify and
select.


### Character selection ###

This will be done by a simple list of character on the left side of
the view. The rest of the view will show the character selected and
the actions you can do with him or her.


### Character and text position ###

This will be simple for buttons, with an easy to identify icon.


### Header bar ###

Header bar will be in 2 parts: One at top of the character selection
list and another on top of the "selected character" part.

The part at top of the character selection list will show 2 buttons:
* Add a character button, symbolized by a "+" icon
* The application burger menu

These 2 buttons will be placed each at one end of their part of the
header bar to clearly separate them, as they don't have the same use.

A click of the button to add a character, simply show an empty
character in edit mode.

The part on top of the "selected character" part will show 3 buttons:
* Start/stop the character rendering, symbolized by a "play" and "stop" icon
* Edit the selected character, symbolized by a "pencil" icon
* Delete the selected character, symbolized by a rubber

The "Start/stop character rendering" button will be placed at the left
end of its part and the two others on the right end. As the first
button don't have the same use as the two other.


### Mockup ###

TODO

## Edition view ##

To edit a character, simply select one and click on edit button.

The edition view is similar to the main, except that the character
name is editable, the emotion grid too, the character selection list
is disabled and the character text entry is replaced by the character
text options.


### Emotions ###

On the emotion grid you can:
* Add or delete emotion
* Choose an emotion emoticon and name
* Choose the character image associated with this emotion


### Text ###

The text section show text option:
* Text size
* Font selection
* Text color
* Background color
* Text speed

### Header bar ###

On the header bar, the part at top of the character selection list is empty.

The part on top of the "selected character" part will show 2 buttons:
* Save
* Cancel


### Mockup ###

TODO


## Deletion confirmation ##

A simple box who ask if we want to delete a character, with 2 buttons:
- Delete
- Cancel
