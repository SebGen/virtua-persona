# Virtua Persona #

When done, this application will let you control a virtual character
that you can include on your live stream.

This character will be able to show differents expressions that you
can select by one click, or one keyboard shortcut. It will also be
able to open and close it's mouth. 

Optionnaly, you will be able to open and close mouth based on your
voice.

Also optionnaly, you will be able to have a text box to expresse
yourself like an old school RPG character.



## Install for use ##

TBD


## Install for dev ##

TBD


## Licence ##

GPLv3


## Author ##

Sébastien Gendre <seb@k-7.ch>
