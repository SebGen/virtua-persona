# User stories #

This document regroupe user stories.

## Story 1 ##

As a: 
User

I want to: 
Create my own character

## Story 2 ##

As a: 
User

I want to: 
Choose my character between a collection

## Story 3 ##

As a: 
User

I want to: 
Have a virtual charater that I can show on a video live stream

## Story 4 ##

As a: 
User

I want to: 
Select background color for the avatar render

## Story 5 ##

As a: 
User

I want to: 
Choose the expression of my virtual character

## Story 6 ##

As a: 
User

I want to: 
Expresse myself with text

## Story 7 ##

As a: 
User

I want to: 
Configure the text rendering (font, size, speed)


## Story 8 ##

As a: 
User

I want to: 
Open and close my avatar mouth manually

## Story 9 ##

As a: 
User

I want to:
Open and close my avatar mouth automatically, based on text
rendered next to it

## Story 10 ##

As a: 
User

I want to: 
Open and close my avatar mouth automatically, based my voice


